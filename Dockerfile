FROM python:3.8-slim

WORKDIR /app

COPY pyproject.toml .
COPY poetry.lock .
COPY app.py .
COPY database.py .
COPY backend.sqlite3 .
COPY main_tests.py .

RUN pip install poetry
RUN poetry install

EXPOSE 80
EXPOSE 3306
EXPOSE 5000

CMD ["poetry", "run", "python3", "app.py"]